﻿using HomeInventory.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HomeInventory.ViewModels
{
    public class HomeItemViewModel
    {
        public int HomeItemId { get; set; }

        [StringLength(100)]
        public string Model { get; set; }

        [StringLength(100)]
        [Display(Name = "Serial Number")]
        public string SerialNumber { get; set; }

        [Display(Name = "Location")]
        public int LocationId { get; set; }

        public Location Location { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }

        public HttpPostedFileBase Photo { get; set; }
        public byte[] PhotoDB { get; set; }

        public PurchaseInfo PurchaseInfo { get; set; }
    }
}